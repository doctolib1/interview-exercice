#! /bin/bash

usage(){
    echo "Provision stuff on k8s cluster" >&2
    echo "--env                  environment name." >&2
    echo "--aws-access-key-id    AWS access key id used by terraform to provision the resources." >&2
    echo "--ws-secret-access-key AWS access key used by terraform to provision the resources." >&2
    echo "--authorized-ip        The authorized IP to access to the EC2 instances." >&2
}

# Parse arguments
#echo "$@"
while [ $# -gt 0 ]; do
  case $1 in
    --env) env=$2 ; shift 2 ;;
    --aws-access-key-id) aws_access_key_id=$2 ; shift 2 ;;
    --aws-secret-access-key) aws_secret_access_key=$2 ; shift 2 ;;
    --authorized-ip) doctolib_authorized_ip=$2 ; shift 2 ;;
    -h|-help|--help) usage;;
    * ) break ;;
  esac
done

terraform init -backend-config="../config/backend_conf_s3"
terraform workspace select ${env} || terraform workspace new ${env}

terraform refresh -var-file="../config/${env}.tfvars" \
  -var="aws_access_key_id=${aws_access_key_id}"  \
  -var="aws_secret_access_key=${aws_secret_access_key}" \
  -var="doctolib_authorized_ip=${doctolib_authorized_ip}"


terraform destroy -var-file="../${env}.tfvars" \
  -var="aws_access_key_id=${aws_access_key_id}"  \
  -var="aws_secret_access_key=${aws_secret_access_key}" \
  -var="doctolib_authorized_ip=${doctolib_authorized_ip}"
