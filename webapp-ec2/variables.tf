variable "aws_region" {
  description = "The AWS region in which resources are set up."
  type        = string
  default     = "eu-west-3"
}

variable "aws_access_key_id" {
  description = "The AWS access key id used by terraform to provision the resources."
  type        = string
}

variable "aws_secret_access_key" {
  description = "The AWS access key used by terraform to provision the resources."
  type        = string
}

variable "doctolib_authorized_ip" {
  description = "The authorized IP to access to the EC2 instances."
  type        = string
  sensitive   = true
}