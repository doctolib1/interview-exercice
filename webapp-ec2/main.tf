locals {
  app_name = "doctolib"
}

data "template_file" "user_data" {
  template = file("${path.module}/conf/docker.yaml")
}

module "instance-setup" {
  source = "git::https://gitlab.com/doctolib1/terraform-modules/doctolib.auto-scaling-web-app/?ref=Q6"
  #  source = "../../modules/doctolib.auto-scaling-web-app"

  # Common configurations
  app_name = local.app_name
  tags = {
    application = local.app_name
    terraform   = "true"
  }

  aws_alb_target_group_arn = module.alb.aws_alb_target_group_arn

  # Placement startegy
  asg_placement_name     = local.app_name
  asg_availability_zones = ["eu-west-3a"]

  # Security configurations
  asg_authorized_ips = [var.doctolib_authorized_ip]

  # EC2 instance configuration
  #  asg_instance_image_id = ""
  #  asg_instance_type     = ""
  asg_instance_user_data = base64encode(data.template_file.user_data.rendered)

  # IAM configuration
  aws_iam_instance_profile = module.s3_write_only.s3_writeonly_role_profile_name
}

module "s3_write_only" {
  source = "git::https://gitlab.com/doctolib1/terraform-modules/doctolib.s3-writeonly/?ref=Q3"

  s3_buckent_name       = "doctolibexercice"
  allowed_object_prefix = "doctolib_can_be_written"

  tags = {
    application = local.app_name
    terraform   = "true"
  }
}

module "alb" {
  source = "git::https://gitlab.com/doctolib1/terraform-modules/doctolib.application-lb/?ref=Q6"
  #  source = "../../modules/doctolib.auto-scaling-web-app"
  tags = {
    application = local.app_name
    terraform   = "true"
  }
  alb_authorized_ips = [var.doctolib_authorized_ip]
  alb_availability_zones = ["eu-west-3a", "eu-west-3b"]
}

resource "aws_autoscaling_policy" "incoming_request" {
  autoscaling_group_name    = module.instance-setup.aws_asg_name
  cooldown                  = 0
  estimated_instance_warmup = 0
  name                      = "incoming_request"
  policy_type               = "TargetTrackingScaling"
  scaling_adjustment        = 0

  target_tracking_configuration {
    disable_scale_in = false
    target_value     = 10

    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${module.alb.aws_alb_suffix}/${module.alb.aws_alb_target_group_arn_suffix}"
    }
  }
}
