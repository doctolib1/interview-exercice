locals {
  project = "doctolib"
}
module "remote_state" {
  source = "git::https://github.com/nozaq/terraform-aws-remote-state-s3-backend/?ref=0.6.0"

  providers = {
    aws         = aws
    aws.replica = aws.replica
  }

  tags = {
    "terraform" : true,
    "project" : local.project
  }

  terraform_iam_policy_name_prefix = local.project
}

resource "aws_iam_user" "terraform" {
  name = var.terraform_aws_iam_user
}

resource "aws_iam_user_policy_attachment" "remote_state_access" {
  user       = aws_iam_user.terraform.name
  policy_arn = module.remote_state.terraform_iam_policy.arn
}
