#! /bin/bash

usage(){
    echo "Provision stuff on k8s cluster" >&2
    echo "--aws-access-key-id    AWS access key id used by terraform to provision the resources." >&2
    echo "--ws-secret-access-key AWS access key used by terraform to provision the resources." >&
}

# Parse arguments
while [ $# -gt 0 ]; do
  case $1 in
    --aws-access-key-id) aws_access_key_id=$2 ; shift 2 ;;
    --aws-secret-access-key) aws_secret_access_key=$2 ; shift 2 ;;
    -h|-help|--help) usage;;
    * ) break ;;
  esac
done

terraform init
terraform workspace select ${env} || terraform workspace new ${env}

terraform refresh \
  -var="aws_access_key_id=${aws_access_key_id}"  \
  -var="aws_secret_access_key=${aws_secret_access_key}"

terraform destroy \
  -var="aws_access_key_id=${aws_access_key_id}"  \
  -var="aws_secret_access_key=${aws_secret_access_key}"