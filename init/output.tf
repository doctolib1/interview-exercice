output "kms_key" {
  description = "The KMS customer master key to encrypt state buckets."
  value       = module.remote_state.kms_key
}

output "kms_key_id" {
  description = "The KMS customer master key to encrypt state buckets."
  value       = module.remote_state.kms_key.id
}

output "state_bucket" {
  description = "The S3 bucket to store the remote state file."
  value       = module.remote_state.state_bucket
}

output "state_bucket_name" {
  description = "The S3 bucket to store the remote state file."
  value       = module.remote_state.state_bucket.bucket
}

output "state_bucket_region" {
  description = "The S3 bucket to store the remote state file."
  value       = module.remote_state.state_bucket.region
}

output "replica_bucket" {
  description = "The S3 bucket to replicate the state S3 bucket."
  value       = module.remote_state.replica_bucket
}

output "dynamodb_table" {
  description = "The DynamoDB table to manage lock states."
  value       = module.remote_state.dynamodb_table
}

output "terraform_iam_policy" {
  description = "The IAM Policy to access remote state environment."
  value       = module.remote_state.terraform_iam_policy
}

output "terraform_iam_user" {
  description = "The IAM user created."
  value       = var.terraform_aws_iam_user
}