variable "aws_region" {
  description = "The AWS region in which resources are set up."
  type        = string
  default     = "eu-west-3"
}

variable "aws_replica_region" {
  description = "The AWS region to which the state bucket is replicated."
  type        = string
  default     = "eu-west-1"
}

variable "aws_access_key_id" {
  description = "The AWS access key id used by terraform to provision the resources."
  type        = string
}

variable "aws_secret_access_key" {
  description = "The AWS access key used by terraform to provision the resources."
  type        = string
}

variable "terraform_aws_iam_user" {
  description = "The aws IAM user that will be used by terraform."
  type        = string
  default     = "doctolib-terraform"
}